package com.za.code;
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

//package components;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.*;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;


public class MainForm extends JPanel implements ActionListener {
	// 
    //JCheckBox chinButton;
    //JCheckBox glassesButton;
    //JCheckBox hairButton;
    //JCheckBox teethButton;


    //StringBuffer choices;
    //JLabel pictureLabel;
    
/*
    private final ExecutorService pool = Executors.newFixedThreadPool(10);
    
    public Future<String> startGetStockPrice(final String symbol) throws IOException {
        return pool.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
            	StockBean stock = StockTickerDAO.getInstance().getStockPrice(symbol);
            	return String.valueOf(stock.getPrice());
            }
        });
    }*/
	javax.swing.Timer timer = new Timer(20, (ActionListener) this);
    ArrayList<StockLoader> contentsFutureList2 = new ArrayList<StockLoader>();
    StockReader sr = null;
    JTable table = null;
    JFrame frame = null;
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MainForm() throws InterruptedException, ExecutionException, IOException {
        super(new BorderLayout());
     // Create the connection factory using the environment variable credential provider.
     // Connections this factory creates can talk to the queues in us-east-1 region. 

        frame = new JFrame("MainForm");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    	contentsFutureList2.add(new StockLoader("NHTC"));
    	contentsFutureList2.add(new StockLoader("ZVV"));
    	contentsFutureList2.add(new StockLoader("EGRX"));
    	contentsFutureList2.add(new StockLoader("NPTN"));
    	contentsFutureList2.add(new StockLoader("PRTA"));
    	contentsFutureList2.add(new StockLoader("ADXSW"));
    	contentsFutureList2.add(new StockLoader("GLPG"));
    	contentsFutureList2.add(new StockLoader("DWTI"));
    	contentsFutureList2.add(new StockLoader("ABCD"));
    	contentsFutureList2.add(new StockLoader("KOLD"));
    	contentsFutureList2.add(new StockLoader("CYTK"));
    	contentsFutureList2.add(new StockLoader("PAM"));
    	contentsFutureList2.add(new StockLoader("HCKT"));
    	contentsFutureList2.add(new StockLoader("DGAZ"));
    	contentsFutureList2.add(new StockLoader("HSKA"));
    	contentsFutureList2.add(new StockLoader("ISLE"));
    	contentsFutureList2.add(new StockLoader("ZIOP"));
    	contentsFutureList2.add(new StockLoader("SPRO"));
    	contentsFutureList2.add(new StockLoader("VLRS"));
    	contentsFutureList2.add(new StockLoader("PRMW"));
    	contentsFutureList2.add(new StockLoader("ANAC"));
    	contentsFutureList2.add(new StockLoader("IMH"));
    	contentsFutureList2.add(new StockLoader("SOCB"));
    	contentsFutureList2.add(new StockLoader("APDNW"));
    	contentsFutureList2.add(new StockLoader("ERI"));
    	contentsFutureList2.add(new StockLoader("EFOI"));
    	contentsFutureList2.add(new StockLoader("LJPC"));
    	contentsFutureList2.add(new StockLoader("TREE"));
    	contentsFutureList2.add(new StockLoader("FIX"));
    	contentsFutureList2.add(new StockLoader("BSQR"));
    	contentsFutureList2.add(new StockLoader("VTL"));
    	contentsFutureList2.add(new StockLoader("AXGN"));
    	contentsFutureList2.add(new StockLoader("INCR"));
    	contentsFutureList2.add(new StockLoader("ANGI"));
    	contentsFutureList2.add(new StockLoader("AMPE"));
    	contentsFutureList2.add(new StockLoader("JNP"));
    	contentsFutureList2.add(new StockLoader("SHEN"));
    	contentsFutureList2.add(new StockLoader("JBLU"));
    	contentsFutureList2.add(new StockLoader("CENT"));
    	contentsFutureList2.add(new StockLoader("EBIX"));
    	contentsFutureList2.add(new StockLoader("BZQ"));
    	contentsFutureList2.add(new StockLoader("CNCE"));
    	contentsFutureList2.add(new StockLoader("BOM"));
    	contentsFutureList2.add(new StockLoader("ROCK"));
    	contentsFutureList2.add(new StockLoader("BZC"));
    	contentsFutureList2.add(new StockLoader("ATVI"));
    	contentsFutureList2.add(new StockLoader("CARA"));
    	contentsFutureList2.add(new StockLoader("MANH"));
    	contentsFutureList2.add(new StockLoader("INTL"));
    	contentsFutureList2.add(new StockLoader("CRWN"));

        timer = new Timer(5000, (ActionListener) this);
        timer.setInitialDelay(10000);
    	timer.start(); 
    	
    	sr = new StockReader();
    	
    	

        
        table = new JTable(new DefaultTableModel(new Object[]{"Symbol", "Price"},0));

        int cnt2 = contentsFutureList2.size();
        for(int i=0; i<cnt2; i++){
        	DefaultTableModel model = (DefaultTableModel) table.getModel();
        	model.addRow(new Object[]{contentsFutureList2.get(i).Symbol(), ""});
        }
        
        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);
        frame.setSize(300, 150);
        frame.setVisible(true);
        //frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }


    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     * @throws IOException 
     * @throws ExecutionException 
     * @throws InterruptedException 
     */
    private static void createAndShowGUI() throws InterruptedException, ExecutionException, IOException {
        //Create and set up the window.

        //Create and set up the content pane.
        JComponent newContentPane = new MainForm();
        newContentPane.setOpaque(true); //content panes must be opaque
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
       javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
					createAndShowGUI();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });
    }



	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		//return;
		
    	if (!sr.isDone()) {return;}
    	DefaultTableModel model = (DefaultTableModel) table.getModel();
    	int cnt = model.getRowCount();
    	

    	int donecnt = 0;
        for(int i=0; i < cnt; i++)
		{
        	if(contentsFutureList2.get(i).isDone()) {donecnt += 1;}
		}
        for(int i=0; i < cnt; i++)
		{
			try {
				String sym = model.getValueAt(i, 0).toString();
	    		HashMap<String, String> r = sr.Results();
		    	String res = r.get(sym);
			    //System.out.println("size1: " + sr.Results().keySet().size() + "; Done Count: " + donecnt);
			    //System.out.println("sym: " + sym + "; res: " + res);
		    	
				model.setValueAt(res, i, 1);
				model.fireTableCellUpdated(i, 1);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		model.fireTableDataChanged();
        try {
			sr.refresh();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}