package com.za.code;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

public class StockLoader {
    private final ExecutorService pool = Executors.newFixedThreadPool(10);
    
    //ExecutorService threadPool = Executors.newFixedThreadPool(4);
    //CompletionService<String> pool2 = new ExecutorCompletionService<String>(threadPool);
    public Future<String> stockPrice = null;
    public String results = null;
    public String symbol = null;
    
    public StockLoader(String symbolIn) throws IOException{
    	symbol = symbolIn;
    	stockPrice = startGetStockPrice();
    }
    
    public Future<String> startGetStockPrice() throws IOException {
        return pool.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
            	StockBean stock = StockTickerDAO.getInstance().getStockPrice(symbol);
            	saveSolr(stock);
            	return String.valueOf(stock.getPrice());
            }
        });
    }
    
    //Future<String> f = future(new Callable<String>() {
    //	  public String call() {
    //	    return "Hello" + "World";
    //	  }
    //	}, system.dispatcher());
    //	 
    //	f.onSuccess(new PrintResult<String>(), system.dispatcher());
    
    public void refresh() throws IOException{
	    if(isDone())
	    	stockPrice = startGetStockPrice();
    }
    
    public void saveSolr(StockBean stock) throws InterruptedException, ExecutionException, SolrServerException, IOException{
		HttpSolrServer server = new HttpSolrServer("http://localhost:8984/solr");
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("cat", "stock");
		doc.addField("id", symbol);
		doc.addField("name", String.valueOf(stock.getPrice()));
		server.add(doc);
		server.commit();
    }
    
    public boolean isDone(){
    	if(stockPrice==null){return true;}
    	return stockPrice.isDone();
    }
    
    public String Results() throws InterruptedException, ExecutionException{
    	if(!isDone()){return null;}
    	if(stockPrice != null){
    		results = stockPrice.get();
    		stockPrice = null;
		}
    	return results;
    }
    
    public String Symbol(){
    	return symbol;
    }
}
