package com.za.code;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

import java.net.MalformedURLException;

import java.io.IOException;

public class Demo {
	public static void main(String[] args)  throws IOException, SolrServerException {
		System.out.println("executing Demo ...");
		//DeleteTest();
		AddTest();
		//QueryTest();
	  }
	
	public static void DeleteTest() throws IOException, SolrServerException {
		HttpSolrServer server = new HttpSolrServer("http://localhost:8984/solr");
		server.deleteByQuery( "*:*" );
	}
	
	public static void AddTest() throws IOException, SolrServerException {
		HttpSolrServer server = new HttpSolrServer("http://localhost:8984/solr");
	    for(int i=0;i<55;++i) {
	      SolrInputDocument doc = new SolrInputDocument();
	      doc.addField("cat", "book");
	      doc.addField("id", "book-" + String.format("%05d", i));
	      doc.addField("name", "The Legend of the Hobbit part " + i);
	      server.add(doc);
	      if(i%100==0) server.commit();  // periodically flush
	    }
	    server.commit(); 
	}
	
	public static void QueryTest() throws MalformedURLException, SolrServerException {
	    //HttpSolrServer server = new HttpSolrServer("http://localhost:8984/solr");

		HttpSolrServer server = new HttpSolrServer("http://localhost:8984/solr");
	    SolrQuery query = new SolrQuery();
	    query.setQuery("*:*");
	    query.addFilterQuery("cat:stock");
	    query.setFields("cat","id","name");
	    query.setStart(0); 
	    query.addSort( "id", SolrQuery.ORDER.asc );
	    query.setRows(9999);
	    //query.set("defType", "edismax");

	    QueryResponse response = server.query(query);
	    SolrDocumentList results = response.getResults();
	    for (int i = 0; i < results.size(); ++i) {
	      System.out.println(results.get(i));
	    }
	  }
}
