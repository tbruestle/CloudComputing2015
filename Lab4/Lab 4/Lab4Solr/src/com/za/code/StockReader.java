package com.za.code;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

public class StockReader {
    private final ExecutorService pool = Executors.newFixedThreadPool(10);
    Future<SolrDocumentList> doclist;

    
    public StockReader() throws IOException{
    }
    
    public Future<SolrDocumentList> startReadList() throws IOException {
        return pool.submit(new Callable<SolrDocumentList>() {
            @Override
            public SolrDocumentList call() throws Exception {
            	return loadSolr();
            }
        });
    }
    
    public void refresh() throws IOException{
	    if(isDone())
	    	doclist = startReadList();
    }
    
    public SolrDocumentList loadSolr() throws InterruptedException, ExecutionException, SolrServerException, IOException{
		HttpSolrServer server = new HttpSolrServer("http://localhost:8984/solr");
	    SolrQuery query = new SolrQuery();
	    query.setQuery("*:*");
	    query.addFilterQuery("cat:stock");
	    query.setFields("cat","id","name");
	    query.setStart(0); 
	    query.setRows(999999);
	    //query.set("defType", "edismax");

	    QueryResponse response = server.query(query);
	    return response.getResults();
    }
    
    public boolean isDone(){
    	if(doclist==null){return true;}
    	return doclist.isDone();
    }
    
    public HashMap<String, String> Results() throws InterruptedException, ExecutionException{
    	if(!isDone()){return new HashMap<String, String>();}
    	if(doclist != null){
    		SolrDocumentList s = doclist.get();
    		int cnt = s.size();
		    //System.out.println("Read Count: " + cnt);
	    	
    		HashMap<String, String> r = new HashMap<String, String>();
		    //System.out.println("Read Count 2: " + cnt);
		    for (int i = 0; i < cnt; ++i) {
			  //System.out.println("Read Num: " + s.get(i));
		      r.put(s.get(i).getFieldValue("id").toString(), s.get(i).getFieldValue("name").toString());
		    }
	    	return r;
		}
    	return new HashMap<String, String>();
    }
}
